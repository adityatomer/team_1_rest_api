package com.gwynniebee.inventoryPackage.restlet.resources;


/**
 * Copyright 2012 GwynnieBee Inc.
 */


import java.util.List;

import org.restlet.resource.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;
import com.gwynniebee.inventoryPackage.response.getRequestDetailsResponse;

import com.gwynniebee.inventoryPackage.tables.RequestDetail;


public class getRequestDetailsResource extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(getRequestDetailsResource.class);

    

    /**
     * Implementation of a GET method.
     * @return "hello world" {String}.
     * @throws ClassNotFoundException 
     */
    @Get
    public getRequestDetailsResponse sayHelloWorld() throws ClassNotFoundException {
        LOG.debug("I am trying to get all Request Details");
        getRequestDetailsResponse response = new getRequestDetailsResponse();
        List<RequestDetail> details= null;
        
            //types = getTypeListEntity.getInstance().getAllType();
           // response.setTypes(types);
           // response.setStatus(this.getRespStatus());
        

        LOG.info("CarrierListServerResource. Request: Get all carrier. Response: " + response);
        
        
        
        
        return response;
    }
}
