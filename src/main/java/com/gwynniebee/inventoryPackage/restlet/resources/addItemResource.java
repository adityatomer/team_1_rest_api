package com.gwynniebee.inventoryPackage.restlet.resources;




/**
 * Copyright 2012 GwynnieBee Inc.
 */


import java.util.List;

import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.barcode.upload.common.Constants;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

import com.gwynniebee.inventoryPackage.response.getProductDetailsResponse;
import com.gwynniebee.inventoryPackage.response.getTypeResponse;
import com.gwynniebee.inventoryPackage.response.myResponse;
import com.gwynniebee.inventoryPackage.entityManager.getTypeListEntity;
import com.gwynniebee.inventoryPackage.restlet.inventoryMangement;
import com.gwynniebee.inventoryPackage.tables.Item;
import com.gwynniebee.inventoryPackage.tables.Type;


public class addItemResource extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(addItemResource.class);

    /*
     * (non-Javadoc)
     * @see org.restlet.resource.UniformResource#doInit()
     */
    @Override
    protected void doInit() {
        super.doInit();
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailSubject()
     */
    @Override
    protected String getErrorEmailSubject() {
        return "Shipment Tracking Service - NetsuiteFileServerResource Error";
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailFrom()
     */
    @Override
    protected String getErrorEmailFrom() {
        return ((inventoryMangement) this.getApplication()).getServiceProperties().getProperty(Constants.ALERT_EMAIL_FROM_PROP);
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailTo()
     */
    @Override
    protected String getErrorEmailTo() {
        return ((inventoryMangement) this.getApplication()).getServiceProperties().getProperty(Constants.ALERT_EMAIL_TO_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailFrom() {
        return ((inventoryMangement) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_FROM_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailTo() {
        return ((inventoryMangement) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_TO_PROP);
    }

    /**
     * Implementation of a GET method.
     * @return "hello world" {String}.
     */
    @Post
    public myResponse addItem(Item item) {
        LOG.debug("trying to return response to addItem");
        myResponse response = new myResponse();
        
        getRespStatus().setCode(ResponseStatus.CODE_SUCCESS);
        getRespStatus().setMessage("Item added");
        setStatus(Status.SUCCESS_ACCEPTED);
        
        
       response.setStatus(getRespStatus());        
           
        

        LOG.info("CarrierListServerResource. Request: Get all carrier. Response: " + response);
        
        
        
        
        return response;
    }
}