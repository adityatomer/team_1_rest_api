package com.gwynniebee.inventoryPackage.restlet.resources;


/**
 * Copyright 2012 GwynnieBee Inc.
 */


import java.util.List;

import org.restlet.resource.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;
import com.gwynniebee.inventoryPackage.response.getTypeResponse;
import com.gwynniebee.inventoryPackage.entityManager.getTypeListEntity;
import com.gwynniebee.inventoryPackage.tables.Type;


public class getTypeList extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(getTypeList.class);

    

    /**
     * Implementation of a GET method.
     * @return "hello world" {String}.
     * @throws ClassNotFoundException 
     */
    @Get
    public getTypeResponse sayHelloWorld() throws ClassNotFoundException {
        LOG.debug("I am trying to say hello world");
        getTypeResponse response = new getTypeResponse();
        List<Type> types= null;
        
            types = getTypeListEntity.getInstance().getAllType();
            response.setTypes(types);
            response.setStatus(this.getRespStatus());
        

        LOG.info("CarrierListServerResource. Request: Get all carrier. Response: " + response);
        
        
        
        
        return response;
    }
}
