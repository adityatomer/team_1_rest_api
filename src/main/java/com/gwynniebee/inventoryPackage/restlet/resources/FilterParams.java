package com.gwynniebee.inventoryPackage.restlet.resources;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FilterParams {

    List<String> fields;
    String table = "assigned_items";
    Map<String, String> criteria = new HashMap<String, String>();
    int offset;
    int limit;
    
    
    public FilterParams() {
        
        offset=0;
        limit=10000;
       fields = Arrays.asList("*");    
    }
    
    public FilterParams(List<String> fields) {
        super();
        this.fields = fields;
    }
    
    
    
    public FilterParams(List<String> fields, String table) {
        super();
        this.fields = fields;
        this.table = table;
    }

    public void addCriteria(String key, String value){
        criteria.put(key, value);
    }

    public void setOffset(int offset){
        this.offset = offset;
    }
    public void setlimit(int limit){
        this.limit = limit;
    }
    
    public String buildQuery(){
        StringBuilder query = new StringBuilder();
         query.append("select ");
        int i = 1;
        for(String field : fields) {
            query.append(field);
            if(i<fields.size()){
                query.append(", ");
            }
            i++;
           
        }
        
        query.append(" FROM ");
        query.append(table);
        
        if(!criteria.isEmpty()) {
            query.append(" WHERE ");
        }
        
        i=1;
        for(String field : criteria.keySet()) 
        { 
        query.append(field);
        query.append("=");
        query.append(criteria.get(field));
        if(i<criteria.size()) {
            query.append(" AND ");
        }
        i++;
        }
        
        query.append(" LIMIT "+this.limit);
        query.append(" OFFSET "+this.offset);
        
        return query.toString();
    }


   
    

}
