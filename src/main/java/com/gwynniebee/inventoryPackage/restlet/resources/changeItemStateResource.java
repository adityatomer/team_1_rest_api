package com.gwynniebee.inventoryPackage.restlet.resources;




/**
 * Copyright 2012 GwynnieBee Inc.
 */


import java.util.List;

import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

import com.gwynniebee.inventoryPackage.response.myResponse;
import com.gwynniebee.inventoryPackage.entityManager.ItemStateChangeEntity;


import com.gwynniebee.inventoryPackage.tables.itemStateChange;



public class changeItemStateResource extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(changeItemStateResource.class);

    /**
     * Implementation of a post method.
     * @return "hello world" {String}.
     * @throws ClassNotFoundException 
     */
    @Post
    public myResponse changeItemState(itemStateChange item) throws ClassNotFoundException {
        LOG.debug("trying to return response to changeItem State");
        myResponse response = new myResponse();
        ItemStateChangeEntity.getInstance().changeItemStatecall(item);
        
        getRespStatus().setCode(ResponseStatus.CODE_SUCCESS);
        getRespStatus().setMessage("Item state changed");
        setStatus(Status.SUCCESS_ACCEPTED);
        
       
        
        
       response.setStatus(getRespStatus());  

        LOG.info("CarrierListServerResource. Request: Get all carrier. Response: " + response);
        
        
        
        
        return response;
    }
}