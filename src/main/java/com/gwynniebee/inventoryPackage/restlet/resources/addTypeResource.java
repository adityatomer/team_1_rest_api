package com.gwynniebee.inventoryPackage.restlet.resources;

/**
 * Copyright 2012 GwynnieBee Inc.
 */

import org.restlet.data.Status;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

import com.gwynniebee.inventoryPackage.response.myResponse;
import com.gwynniebee.inventoryPackage.entityManager.AddTypeEntity;
import com.gwynniebee.inventoryPackage.tables.addType;


public class addTypeResource extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(addTypeResource.class);


    /**
     * Implementation of a GET method.
     * @return "hello world" {String}.
     * @throws ClassNotFoundException 
     */
    @Post
    public myResponse addType(addType type) throws ClassNotFoundException {
        LOG.debug("trying to return response to addTypes");
        
        myResponse response = new myResponse();
        
        
        int status = AddTypeEntity.getInstance().addTypecall(type);
        
        getRespStatus().setCode(status);
        getRespStatus().setMessage("Item added");
        setStatus(Status.SUCCESS_ACCEPTED);
        
        
       response.setStatus(getRespStatus());  
        

        LOG.info("Add a new type" + response);
        
        
        
        
        return response;
    }
}