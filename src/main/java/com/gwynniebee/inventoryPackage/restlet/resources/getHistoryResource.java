package com.gwynniebee.inventoryPackage.restlet.resources;


/**
 * Copyright 2012 GwynnieBee Inc.
 */


import java.util.List;

import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;
import com.gwynniebee.inventoryPackage.entityManager.HistoryEntityManager;
import com.gwynniebee.inventoryPackage.response.getHistoryResponse;
import com.gwynniebee.inventoryPackage.tables.ItemHistory;



public class getHistoryResource extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(getHistoryResource.class);

    /**
     * Implementation of a GET method.
     * @return "hello world" {String}.
     * @throws ClassNotFoundException 
     */
    @Get
    public getHistoryResponse getHistory(ItemHistory itemhistory) throws ClassNotFoundException {
        LOG.debug("trying to return response to getHistory");
        getHistoryResponse response = new getHistoryResponse();
        List<ItemHistory> history = null;
        FilterParams qb = ServerUtils.prepareGetParams(this);
        history =  HistoryEntityManager.getInstance().getHistory(qb, 0, 0);
        
        getRespStatus().setCode(ResponseStatus.CODE_SUCCESS);
        getRespStatus().setMessage("Item added");
        setStatus(Status.SUCCESS_ACCEPTED);
        
        
       response.setStatus(getRespStatus());  
       
       response.setHistory(history);

        LOG.info("CarrierListServerResource. Request: Get all carrier. Response: " + response);
        
        
        
        
        return response;
    }
}