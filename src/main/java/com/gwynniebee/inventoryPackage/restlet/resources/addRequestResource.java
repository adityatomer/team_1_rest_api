package com.gwynniebee.inventoryPackage.restlet.resources;




/**
 * Copyright 2012 GwynnieBee Inc.
 */


import java.util.List;

import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.barcode.upload.common.Constants;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

import com.gwynniebee.inventoryPackage.response.getProductDetailsResponse;
import com.gwynniebee.inventoryPackage.response.getTypeResponse;
import com.gwynniebee.inventoryPackage.response.myResponse;
import com.gwynniebee.inventoryPackage.entityManager.AddRequestEntity;
import com.gwynniebee.inventoryPackage.entityManager.ItemStateChangeEntity;
import com.gwynniebee.inventoryPackage.entityManager.getTypeListEntity;
import com.gwynniebee.inventoryPackage.restlet.inventoryMangement;
import com.gwynniebee.inventoryPackage.tables.Request;



public class addRequestResource extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(addRequestResource.class);

    

    /**
     * Implementation of a POST method.
     * @throws ClassNotFoundException 
     * 
     */
    @Post
    public myResponse addRequest(Request request) throws ClassNotFoundException {
        LOG.debug("trying to return response to addRequest");
        
        
        myResponse response = new myResponse();
        
        
        
        AddRequestEntity.getInstance().requestItemCall(request);
        
        
        getRespStatus().setCode(ResponseStatus.CODE_SUCCESS);
        getRespStatus().setMessage("request added");
        setStatus(Status.SUCCESS_ACCEPTED);
        
        
       response.setStatus(getRespStatus());  
        

        LOG.info("CarrierListServerResource. Request: Get all carrier. Response: " + response);
        
        
        
        
        return response;
    }
}