package com.gwynniebee.inventoryPackage.restlet.resources;

import org.restlet.data.Form;

import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

public class ServerUtils {
    
    public static FilterParams prepareGetParams(AbstractServerResource asr)
    {
        FilterParams qb = new FilterParams();
        
        String state =  getParamValue(asr, "assignedStatus");
        if (state != null && state.length()>0)
        {
            qb.addCriteria("assignedStatus", state);
        }
        
        String temp = getParamValue(asr,"offset");
        
        if(temp!=null)
        {
            int offset = Integer.parseInt(temp);
            qb.setOffset(offset);
        }
        
        temp = getParamValue(asr,"limit");
        
        if(temp!=null)
        {
            int limit = Integer.parseInt(temp);
            qb.setlimit(limit);
        }
        
        return qb;
    }

    private static String getParamValue(AbstractServerResource serverResource, String key) {
        Form queryParams = serverResource.getRequest().getResourceRef().getQueryAsForm();
        String value = queryParams.getFirstValue(key);
        return value;
    }
}
