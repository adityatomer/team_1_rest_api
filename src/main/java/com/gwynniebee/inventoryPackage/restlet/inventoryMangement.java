/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryPackage.restlet;

import java.util.Properties;


import javax.validation.Validator;

import org.restlet.Application;
import org.restlet.data.MediaType;
import org.restlet.routing.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.rest.service.restlet.GBRestletApplication;




import com.gwynniebee.inventoryPackage.restlet.resources.addItemResource;
import com.gwynniebee.inventoryPackage.restlet.resources.addRequestResource;
import com.gwynniebee.inventoryPackage.restlet.resources.addTypeResource;
import com.gwynniebee.inventoryPackage.restlet.resources.changeItemIdResource;

import com.gwynniebee.inventoryPackage.restlet.resources.changeItemStateResource;
import com.gwynniebee.inventoryPackage.restlet.resources.getHistoryResource;
import com.gwynniebee.inventoryPackage.restlet.resources.getProductDetailsResource;
import com.gwynniebee.inventoryPackage.restlet.resources.getRequestDetailsResource;
import com.gwynniebee.inventoryPackage.restlet.resources.getTypeList;
import com.gwynniebee.inventoryPackage.restlet.resources.updateProductResource;

/**
 * Barcode file upload service routing and controlling resources.
 * @author Sarath
 */

public class inventoryMangement extends GBRestletApplication {

    private static final Logger LOG = LoggerFactory.getLogger(inventoryMangement.class);
    private static Validator validator;
    private Properties serviceProperties;

    public static inventoryMangement getCurrent() {
        return (inventoryMangement) Application.getCurrent();
    }
    

    /*
     * @return
     * @see org.restlet.Application#createInboundRoot()
     */
    @Override
    public synchronized Router createInboundRoot() {

        Router router = super.createInboundRoot();
        String resourceUrl = null;
        this.getMetadataService().setDefaultMediaType(MediaType.APPLICATION_JSON);

        resourceUrl = "/types.json";
        router.attach(resourceUrl, getTypeList.class);
        LOG.debug("Attaching gettypes with " + resourceUrl);
        //done(chaitanya)
        
        resourceUrl = "/productId.json";
        router.attach("/productId.json",getProductDetailsResource.class);  
        LOG.debug("Attaching getproductdetails with "+ resourceUrl);
        //(aditya)
        
        resourceUrl = "/items.json";
        router.attach(resourceUrl,addItemResource.class);  
        LOG.debug("Attaching addItem with "+ resourceUrl);
        //(aditya)
        
        resourceUrl = "/addTypes.json";
        router.attach(resourceUrl,addTypeResource.class);  
        LOG.debug("Attaching addTypes with "+ resourceUrl);
        //done(chaitanya)
        
        resourceUrl = "/updateProducts.json";
        router.attach(resourceUrl,updateProductResource.class);  
        LOG.debug("Attaching updateProduct with "+ resourceUrl);
        //partially done-without optional parameters(chaitanya)
        
        resourceUrl = "/changeitemState.json";
        router.attach(resourceUrl,changeItemStateResource.class);  
        LOG.debug("Attaching changeItemState with "+ resourceUrl);
        //done(harsha)-change date functions
        
        resourceUrl = "/request.json";
        router.attach(resourceUrl,addRequestResource.class);  
        LOG.debug("Attaching addRequest with "+ resourceUrl);
        //done(harsha)-change date functions
        
        resourceUrl = "/allRequestDetails.json";
        router.attach(resourceUrl,getRequestDetailsResource.class);  
        LOG.debug("Attaching getRequestDetails with "+ resourceUrl);
        //bonus(harsha)
        
        resourceUrl = "/history.json";
        router.attach(resourceUrl,getHistoryResource.class);  
        LOG.debug("Attaching getHistory with "+ resourceUrl);
        //done(chaitanya)
        
        return router;
    }


    

    /*
     * (non-Javadoc)
     * @see org.restlet.Application#stop()
     */
    @Override
    public synchronized void stop() throws Exception {
        if (!this.isStopped()) {
            LOG.info("Application.stop()");
        }
        // below will make this.isStopped() true
        super.stop();
    }

    /**
     * @return the serviceProps
     */
    public Properties getServiceProperties() {
        return this.serviceProperties;
    }

    /**
     * @return validator
     */
    public static Validator getValidator() {
        return validator;
    }
}
