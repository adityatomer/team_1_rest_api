/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.inventoryPackage.j2ee;

import com.gwynniebee.rest.service.j2ee.GBRestletServlet;
import com.gwynniebee.inventoryPackage.restlet.inventoryMangement;

/**
 * @author sarath
 */
@SuppressWarnings("serial")
public class RestfulServlet extends GBRestletServlet<inventoryMangement> {
    /**
     * This is called by tomcat's start of servlet.
     */
    public RestfulServlet() {
        this(new inventoryMangement());
    }

    /**
     * This is called by unit test which create a subclass of this class with subclass of application.
     * @param app application
     */
    public RestfulServlet(inventoryMangement app) {
        super(app);
    }
}
