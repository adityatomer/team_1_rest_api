


package com.gwynniebee.inventoryPackage.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.inventoryPackage.mapper.ItemStateChangeMapper;


@RegisterMapper(ItemStateChangeMapper.class)
public interface ItemStateChangeDao extends Transactional<ItemStateChangeDao>{
    /**
     * Get all types.
     * @return list of all types
     */
  
    @SqlUpdate("UPDATE inventory  SET idState=:state  WHERE  itemId= :itemId")
        void changeState(@Bind("state") int state,@Bind("itemId") int itemId);
    

    @SqlUpdate("INSERT INTO assigned_items (itemId, userId,dateAssigned, " +
    		"expectedReturn,assignedStatus,returnDate) values (:itemId,:userId,'2014-7-22','2014-8-01',2,'0000-00-00') ")
    void addRowAssignedItems(@Bind("itemId") int itemId,@Bind("userId") String userId);
    
    
    @SqlUpdate("UPDATE assigned_items SET returnDate='2014-7-23', assignedStatus=1 WHERE itemId=:itemId AND assignedStatus=2")
    void addReturnDate(@Bind("itemId") int itemId);
    
    
    
    
    /**
     * closes the underlying connection.
     */
    void close();
    
    
}