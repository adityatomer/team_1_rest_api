



package com.gwynniebee.inventoryPackage.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.gwynniebee.inventoryPackage.mapper.requestDetailsmapper;

import com.gwynniebee.inventoryPackage.tables.*;


@RegisterMapper(requestDetailsmapper.class)
public interface getRequestDetailsDao  {

    /**
     * Get all types.
     * @return list of all types
     */
    @SqlQuery("select typeId,typeName from type")
    List<Type> getAllTypes();
    
    
    /**
     * closes the underlying connection.
     */
    void close();
    
}