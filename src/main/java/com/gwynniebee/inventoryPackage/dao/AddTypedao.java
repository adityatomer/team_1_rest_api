package com.gwynniebee.inventoryPackage.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.inventoryPackage.mapper.addTypeMapper;

@RegisterMapper(addTypeMapper.class)
public interface AddTypedao extends Transactional<AddTypedao>{
    /**
     * Get all types.
     * @return list of all types
     */
    @SqlUpdate("insert into type (typeName) values (:typeName)")
    @GetGeneratedKeys
    int addType(@Bind("typeName") String typeName );
    
    
    /**
     * closes the underlying connection.
     */
    void close();
    
    
}
