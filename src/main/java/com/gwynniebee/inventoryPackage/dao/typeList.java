package com.gwynniebee.inventoryPackage.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.gwynniebee.inventoryPackage.mapper.typeListMapper;
import com.gwynniebee.inventoryPackage.tables.*;


@RegisterMapper(typeListMapper.class)
public interface typeList {

    /**
     * Get all types.
     * @return list of all types
     */
    @SqlQuery("select typeId,typeName from type")
    List<Type> getAllTypes();
    
    
    /**
     * closes the underlying connection.
     */
    void close();
    
}
