


package com.gwynniebee.inventoryPackage.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

import org.skife.jdbi.v2.sqlobject.mixins.Transactional;




public interface AddRequestDao extends Transactional<AddRequestDao>{
    /**
     * 
     */
  
    @SqlUpdate("INSERT INTO request (userId,productId,approvedState,dateOfRequest) values (:userId,:productId,0,'1991-12-12')")
        void addRequest(@Bind("userId") String userId,@Bind("productId") int productId);
    
    
    /**
     * closes the underlying connection.
     */
    void close();
    
    }