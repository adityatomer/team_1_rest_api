package com.gwynniebee.inventoryPackage.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.inventoryPackage.mapper.updateProductMapper;

@RegisterMapper(updateProductMapper.class)
public interface UpdateProductdao extends Transactional<UpdateProductdao>{
    /**
     * Get all types.
     * @return list of all types
     */
    @SqlUpdate("UPDATE Product  SET " +
    		"productName = :productName, description = :description," +
    		"imageSource = :imageSource where productId = :productId")
    @GetGeneratedKeys
    int updateProduct(@Bind("productId") int productId,@Bind("typeName") String typeName,
            @Bind("productName") String productName,@Bind("description") String description,
            @Bind("imageSource") String imageSource);
    
    
    /**
     * closes the underlying connection.
     */
    void close();
    
    
}
