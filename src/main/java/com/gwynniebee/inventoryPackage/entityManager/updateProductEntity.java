package com.gwynniebee.inventoryPackage.entityManager;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryPackage.constants.inventoryManagementConstants;
import com.gwynniebee.inventoryPackage.dao.UpdateProductdao;
import com.gwynniebee.inventoryPackage.tables.ProductDetails;

public class updateProductEntity {
    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(updateProductEntity.class);
    private static final updateProductEntity MANAGER_INSTANCE = new updateProductEntity();
    
    /**
     * Get all carrier.
     * @return list of all {@link Carrier} objects
     * @throws ClassNotFoundException 
     */
    
    public int updateProduct(ProductDetails product) throws ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        DBI dbi=new DBI(inventoryManagementConstants.DATA_SOURCE_NAME,
                inventoryManagementConstants.DATABASE_USERNAME,
                inventoryManagementConstants.DATABASE_PASSWORD);
        
        UpdateProductdao dao = null;
        Handle h=null;
        h=dbi.open();
        h.begin();
        dao = h.attach(UpdateProductdao.class);
        
        
        try {
            
            return dao.updateProduct(product.getProductId(),product.getTypeName(),
                    product.getProductName(),product.getDescription(),
                    product.getImageSource());
        } finally {
            if (dao != null) {
                h.commit();
                dao.close();
                
            }
        }
    }
    
    public static updateProductEntity getInstance() {
        return MANAGER_INSTANCE;
    }

}
