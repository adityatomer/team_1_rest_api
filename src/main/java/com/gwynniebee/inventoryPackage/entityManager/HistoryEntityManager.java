
package com.gwynniebee.inventoryPackage.entityManager;


import java.util.List;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.gwynniebee.inventoryPackage.constants.inventoryManagementConstants;
import com.gwynniebee.inventoryPackage.mapper.HistoryMapper;
import com.gwynniebee.inventoryPackage.restlet.resources.FilterParams;
import com.gwynniebee.inventoryPackage.tables.ItemHistory;

public class HistoryEntityManager extends BaseEntityManager {
    
    private static final Logger LOG = LoggerFactory.getLogger(getTypeListEntity.class);
    private static final HistoryEntityManager MANAGER_INSTANCE = new HistoryEntityManager();
    
    
    
    /**
     * Get history.
     * @return list of all {@link Carrier} objects
     * @throws ClassNotFoundException 
     */
    
    public List<ItemHistory> getHistory(FilterParams filterParams,int offset,int limit) throws ClassNotFoundException {
        List<ItemHistory> history;
        Handle h = null;
        Class.forName("com.mysql.jdbc.Driver");
        DBI dbi=new DBI(inventoryManagementConstants.DATA_SOURCE_NAME,
                inventoryManagementConstants.DATABASE_USERNAME,
                inventoryManagementConstants.DATABASE_PASSWORD);
        
        try {
        String query = filterParams.buildQuery();
        LOG.info("Query -> " + query);
        h = dbi.open();           
        LOG.debug("inside gethistory with filtering.");         
       
        history = h.createQuery(query).map(new HistoryMapper()).list();
        LOG.debug("Got inventory : " + history.toString());
       
        }finally {
            if (h != null) {
                h.close();
            }
        }
        
        return history;
    }
    
    public static HistoryEntityManager getInstance() {
        return MANAGER_INSTANCE;
    }

}
