package com.gwynniebee.inventoryPackage.entityManager;

import java.util.List;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.inventoryPackage.constants.inventoryManagementConstants;
import com.gwynniebee.inventoryPackage.dao.typeList;
import com.gwynniebee.inventoryPackage.tables.*;


public class getTypeListEntity extends BaseEntityManager{


    private static final Logger LOG = LoggerFactory.getLogger(getTypeListEntity.class);
    private static final getTypeListEntity MANAGER_INSTANCE = new getTypeListEntity();
    
    
    
    /**
     * Get all carrier.
     * @return list of all {@link Carrier} objects
     * @throws ClassNotFoundException 
     */
    
    public List<Type> getAllType() throws ClassNotFoundException {
        typeList dao = null;
        Handle h=null;
        Class.forName("com.mysql.jdbc.Driver");
        DBI dbi=new DBI(inventoryManagementConstants.DATA_SOURCE_NAME,
                inventoryManagementConstants.DATABASE_USERNAME,
                inventoryManagementConstants.DATABASE_PASSWORD);
        h=dbi.open();
        h.begin();
        dao = h.attach(typeList.class);
        try {
            dao = dbi.open(typeList.class);
            return dao.getAllTypes();
        } finally {
            if (dao != null) {
                h.commit();
                dao.close();
            }
        }
    }
    
    public static getTypeListEntity getInstance() {
        return MANAGER_INSTANCE;
    }

}
