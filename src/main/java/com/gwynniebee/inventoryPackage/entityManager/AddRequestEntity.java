


package com.gwynniebee.inventoryPackage.entityManager;


import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gwynniebee.inventoryPackage.constants.inventoryManagementConstants;


import com.gwynniebee.inventoryPackage.dao.AddRequestDao;

import com.gwynniebee.inventoryPackage.tables.Request;

public class  AddRequestEntity  extends BaseEntityManager{

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(AddRequestEntity.class);
    private static final AddRequestEntity MANAGER_INSTANCE = new AddRequestEntity();
    
    /**
     * Get all carrier.
     * @return list of all {@link Carrier} objects
     * @throws ClassNotFoundException 
     */
    
    public void requestItemCall(Request request) throws ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        DBI dbi=new DBI(inventoryManagementConstants.DATA_SOURCE_NAME,
                inventoryManagementConstants.DATABASE_USERNAME,
                inventoryManagementConstants.DATABASE_PASSWORD);
        
        AddRequestDao dao = null;
        Handle h=null;
        h=dbi.open();
        h.begin();
        dao = h.attach(AddRequestDao.class);
        try {
            
             dao.addRequest(request.userId,request.productId);
        } finally {
            if (dao != null) {
                h.commit();
                dao.close();
                
            }
        }
    }
    
    public static AddRequestEntity getInstance() {
        return MANAGER_INSTANCE;
    }
}