

package com.gwynniebee.inventoryPackage.entityManager;


import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gwynniebee.inventoryPackage.constants.inventoryManagementConstants;


import com.gwynniebee.inventoryPackage.dao.ItemStateChangeDao;

import com.gwynniebee.inventoryPackage.tables.itemStateChange;

public class ItemStateChangeEntity extends BaseEntityManager{

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(ItemStateChangeEntity.class);
    private static final ItemStateChangeEntity MANAGER_INSTANCE = new ItemStateChangeEntity();
    
    /**
     * Get all carrier.
     * @return list of all {@link Carrier} objects
     * @throws ClassNotFoundException 
     */
    
    public void changeItemStatecall(itemStateChange state) throws ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        DBI dbi=new DBI(inventoryManagementConstants.DATA_SOURCE_NAME,
                inventoryManagementConstants.DATABASE_USERNAME,
                inventoryManagementConstants.DATABASE_PASSWORD);
        
        ItemStateChangeDao dao = null;
        Handle h=null;
        h=dbi.open();
        h.begin();
        dao = h.attach(ItemStateChangeDao.class);
        try {
            
             dao.changeState(state.state,state.itemId); 
            
            if (state.state==2) {dao.addRowAssignedItems(state.itemId, state.userId);}//being assigned }
             if (state.state==1) {dao.addReturnDate(state.itemId);} //being returned}
        } finally {
            if (dao != null) {
                h.commit();
                dao.close();
                
            }
        }
    }
    
    public static ItemStateChangeEntity getInstance() {
        return MANAGER_INSTANCE;
    }
}