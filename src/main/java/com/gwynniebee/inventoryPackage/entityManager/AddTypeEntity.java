package com.gwynniebee.inventoryPackage.entityManager;


import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gwynniebee.inventoryPackage.constants.inventoryManagementConstants;

import com.gwynniebee.inventoryPackage.dao.AddTypedao;
import com.gwynniebee.inventoryPackage.tables.addType;

public class AddTypeEntity extends BaseEntityManager{

    @SuppressWarnings("unused")
    private static final Logger LOG = LoggerFactory.getLogger(AddTypeEntity.class);
    private static final AddTypeEntity MANAGER_INSTANCE = new AddTypeEntity();
    
    /**
     * Get all carrier.
     * @return list of all {@link Carrier} objects
     * @throws ClassNotFoundException 
     */
    
    public int addTypecall(addType type) throws ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        DBI dbi=new DBI(inventoryManagementConstants.DATA_SOURCE_NAME,
                inventoryManagementConstants.DATABASE_USERNAME,
                inventoryManagementConstants.DATABASE_PASSWORD);
        
        AddTypedao dao = null;
        Handle h=null;
        h=dbi.open();
        h.begin();
        dao = h.attach(AddTypedao.class);
        try {
            
            return dao.addType(type.getTypeName());
        } finally {
            if (dao != null) {
                h.commit();
                dao.close();
                
            }
        }
    }
    
    public static AddTypeEntity getInstance() {
        return MANAGER_INSTANCE;
    }
}
