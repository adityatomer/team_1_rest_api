package com.gwynniebee.inventoryPackage.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.inventoryPackage.tables.*;

public class typeListMapper implements ResultSetMapper<Type>{
    @Override
    public Type map(int index, ResultSet r, StatementContext ctx) throws SQLException {

        Type type = new Type();

        type.setTypeId(r.getInt("typeId"));
        type.setTypeName(r.getString("typeName"));

        return type;
    }

}
