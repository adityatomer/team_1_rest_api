package com.gwynniebee.inventoryPackage.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.inventoryPackage.tables.ItemHistory;;

public class HistoryMapper implements ResultSetMapper<ItemHistory> {

    @Override
    public ItemHistory map(int arg0, ResultSet arg, StatementContext arg2) throws SQLException {
        
        
        ItemHistory history = new ItemHistory();
        ResultSetMetaData metadata = arg.getMetaData();
      
        for (int i = 1; i <= metadata.getColumnCount(); ++i) {

            String name = metadata.getColumnLabel(i);

            if (name.equalsIgnoreCase("assignid")) {
                history.setAssignId(arg.getInt("assignid"));
            } else if (name.equalsIgnoreCase("itemid")) {
                history.setItemId(arg.getInt("itemid"));
            } else if (name.equalsIgnoreCase("assignedstatus")) {
                history.setassignedStatus(arg.getInt("assignedstatus"));
            }  else if (name.equalsIgnoreCase("userid")) {
                history.setUserId(arg.getString("userid"));
            } else if (name.equalsIgnoreCase("dateAssigned")) {
                history.setDateAssigned(arg.getDate("dateAssigned"));
            }
            else if (name.equalsIgnoreCase("expectedDate")) {
                history.setExpectedDate(arg.getDate("expectedDate"));
            }else if (name.equalsIgnoreCase("returnDate")) {
                history.setReturnDate(arg.getDate("returnDate"));
            }
        }
  
        return history;
        
    }

}
