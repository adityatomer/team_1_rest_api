package com.gwynniebee.inventoryPackage.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.inventoryPackage.tables.addType;

public class addTypeMapper implements ResultSetMapper<addType>{
    
    @Override
    public addType map(int index, ResultSet r, StatementContext ctx) throws SQLException {

        addType type = new addType();

        
        type.setTypeName(r.getString("typeName"));
        

        return type;
    }

}
