


package com.gwynniebee.inventoryPackage.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.inventoryPackage.tables.*;

public class requestDetailsmapper implements ResultSetMapper<RequestDetail>{
    @Override
    public RequestDetail map(int index, ResultSet r, StatementContext ctx) throws SQLException {

        RequestDetail detail = new RequestDetail();

       
        
        detail.setPendingRequests(r.getInt("pendingRequests"));
        detail.setProductId(r.getInt("productId"));
        detail.setProductName(r.getString("productName"));
        detail.setStock(r.getInt("stock"));
        detail.setTypeId(r.getInt("typeId"));
       detail.setTypeName(r.getString("typeName")); 
        return detail;
    }

}