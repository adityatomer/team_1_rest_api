package com.gwynniebee.inventoryPackage.tables;

public class RequestDetail {
	
	String typeName,productName;
	
	int typeId,productId,pendingRequests,stock;  
	
    public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public int getPendingRequests() {
		return pendingRequests;
	}
	public void setPendingRequests(int pendingRequests) {
		this.pendingRequests = pendingRequests;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
 
	

}
