package com.gwynniebee.inventoryPackage.tables;

import java.util.Date;

public class ItemHistory {
	
	 int assignId,assignedStatus;
    public Date getReturnDate() {
        return returnDate;
    }
    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }
    public Date getExpectedDate() {
        return expectedDate;
    }
    public void setExpectedDate(Date expectedDate) {
        this.expectedDate = expectedDate;
    }
    public Date getDateAssigned() {
        return dateAssigned;
    }
    public void setDateAssigned(Date dateAssigned) {
        this.dateAssigned = dateAssigned;
    }
    String userId;
	 Date returnDate,expectedDate,dateAssigned;
	 int itemId;
	 
	 
    public int getAssignId() {
        return assignId;
    }
    public void setAssignId(int assignId) {
        this.assignId = assignId;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public int getassignedStatus() {
        return assignedStatus;
    }
    public void setassignedStatus(int assignedStatus) {
        this.assignedStatus = assignedStatus;
    }
    
    public int getItemId() {
        return itemId;
    }
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }
}
