package com.gwynniebee.inventoryPackage.tables;



import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class addType {
    @JsonIgnore
	int typeId;
    
	String TypeName;
	
	
   public String getTypeName(){
    	
    	return this.TypeName;    }
   
   public void setTypeName(String TypeName){
   	
   	 this.TypeName= TypeName;    }
   
   
}
