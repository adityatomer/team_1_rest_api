package com.gwynniebee.inventoryPackage.tables;

public class Item {
	
	
	String itemKey,description, imageSource,dateAdded;
	int productId,typeId;
	
public	String getItemKey(){
	
	
	
	return this.itemKey;
} 
public	String getDescrption(){
	
	
	
	return this.description;
} 
public	String getImageSource(){
	
	
	
	return this.imageSource;
} 
public	String getDateAdded(){
	
	
	
	return this.dateAdded;
} 

public int getProductId(){
	return this.productId;
}
	

public int getTypeId(){
	
	return this.typeId;
	
}
public void setDescription(String description){
	
	this.description= description;
	
	
}

public void setImageSource(String imageSource){
	
	this.imageSource= imageSource;
	
	
}

public void setDateAded(String dateAdded){
	
	this.dateAdded=dateAdded;
}

public void setProductId(int productId){
	
	this.productId= productId;
	
}

public void setTypeId(int typeId){
	
	this.typeId= typeId;
	
}
	
	

}
