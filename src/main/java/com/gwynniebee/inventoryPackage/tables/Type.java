package com.gwynniebee.inventoryPackage.tables;

public class Type {

    public int typeId;
    public String typeName;
    
    public void setTypeId(int id)
    {
        this.typeId = id;
    }
    public void setTypeName(String name)
    {
        this.typeName = name;
    }
}
