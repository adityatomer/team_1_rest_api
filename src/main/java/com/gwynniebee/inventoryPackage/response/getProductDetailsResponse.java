package com.gwynniebee.inventoryPackage.response;

import java.util.List;

import com.gwynniebee.inventoryPackage.tables.ProductDetails;
import com.gwynniebee.inventoryPackage.tables.Type;
import com.gwynniebee.rest.common.response.AbstractResponse;

public class getProductDetailsResponse extends AbstractResponse {

	
	 public List<ProductDetails> ProductDetailsList;
	    
	    public List<ProductDetails> getProductDetailsList()
	    {
	        return ProductDetailsList;
	    }
	    
	    public List<ProductDetails> setProductDetailsList()
	    {
	        return ProductDetailsList;
	    }
	    

	
}
