package com.gwynniebee.inventoryPackage.response;

import java.util.List;

import com.gwynniebee.inventoryPackage.tables.ItemHistory;
import com.gwynniebee.rest.common.response.AbstractResponse;

public class getHistoryResponse extends AbstractResponse {

	
	 public List<ItemHistory> history;
	    
	    public List<ItemHistory> getHistory()
	    {
	        return history;
	    }
	    
	    public void setHistory(List<ItemHistory> history)
	    {
	        this.history = history;
	    }

}
