package com.gwynniebee.inventoryPackage.response;

import java.util.List;

import com.gwynniebee.inventoryPackage.tables.Type;
import com.gwynniebee.rest.common.response.AbstractResponse;

public class getTypeResponse extends AbstractResponse{
    
    public List<Type> types;
    
    public List<Type> getTypes()
    {
        return types;
    }
    
    public void setTypes(List<Type> types)
    {
        this.types = types;
    }

}
