package com.gwynniebee.inventoryPackage.response;

import java.util.List;

import com.gwynniebee.inventoryPackage.tables.RequestDetail;

import com.gwynniebee.rest.common.response.AbstractResponse;

public class getRequestDetailsResponse extends AbstractResponse{
	
	 List<RequestDetail> requestDetails;

	public List<RequestDetail> getRequestDetails() {
		return requestDetails;
	}

	public void setRequestDetails(List<RequestDetail> requestDetails) {
		this.requestDetails = requestDetails;
	}
	 
	  
	

}
