package com.gwynniebee.inventoryPackage.constants;

public final class inventoryManagementConstants {


    public static  final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    
    public static final String DATA_SOURCE_NAME = "jdbc:mysql://localhost/inventory?zeroDateTimeBehavior=convertToNull";
    
    public static final String DATABASE_USERNAME = "root";
    
    public static final String DATABASE_PASSWORD = "tiger";
    
    public static final String TYPE_TABLE = "t";
}
